$(document).ready(function(){


// ===========  CAMERA FUNCTION  ==============

    // Grab elements, create settings, etc.
    var video = document.getElementById('video');

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }

 
    function countDown() {
        var divs = $('.countdown');
        var timer
        var offset = 0 

        divs.each(function(){
            var self = this;
            timer = setTimeout(function(){
                $(self).css('font-size', '150px');
                $(self).fadeOut(1000);
            }, 1000 + offset);
            offset += 1000;
        });

        // flash
        setTimeout(function(){
            $('.flash').fadeIn(200);
            $('.flash').fadeOut(400);
            $(divs).css('font-size', '0px');
        }, 4000);
    }


    function gifPhotos() {
        var divs = $('.countdown');
        var offset = 1000

        for ( i=0; i<5; i++) {

            (function(i){
              setTimeout(function(){
                    $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='1200'></canvas></div>")
                        var canvas = $('.canvas');
                        var context = canvas[canvas.length -1].getContext('2d');
                        var video = document.getElementById('video');
                        var logo = $('.logo_card')[0];
                        var back = $('.back')[0];
               
                        $('.flash').fadeIn(200);
                        $('.flash').fadeOut(400);
                        $(divs).css('font-size', '0px');
                        context.drawImage(back, 0, 0, 900, 1200);
                       context.drawImage(video, 0, 0, 900, 675);
                       context.drawImage(logo, 50, 736.5, 800,402);

                       $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');


                    $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))
              
              }, offset);
              offset += 1000
            }(i));  
        }
    }


    function showGifs() {
        var photos = $('#pictures').children('.canvas_holder');
        $('#photos').hide();
        $('#pictures').fadeIn(1000);
        $('.gif_info').fadeIn();
        $('.home').css('position', 'relative');
        // debugger
        for ( i=0; i<6; i++ ) {
            $(photos[i]).css('display', 'inline-block');
        }
    }


    function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }

    function convertCanvasToImage2(canvas) {
        var dataURL = canvas.toDataURL();
        return canvas.src = dataURL;
    }

    


    function showPictures() {
        $('#pictures').show();
        var photos = $('#pictures').children('.photo');
            $(photos[0]).fadeIn();

        $.post('/contact/save_img/',{image:$($(photos[0]).html()).attr('src')},function(data){
            $('#submit_form input[name=image]').val(data.image.id);
        });

        // $('#pen').fadeIn();

    }



    // GIF CLICK
    document.getElementById("snap_gif").addEventListener("click", function() {
        $(this).fadeOut(1000)
        $(this).css('pointer-events', 'none');
        var pics = []

        countDown();
        setTimeout(function(){
            $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='1200'></canvas></div>")                
                var canvas = $('.canvas');
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                var logo = $('.logo_card')[0];
                var back = $('.back')[0];
               
                       
               context.drawImage(back, 0, 0, 900, 1200);
               context.drawImage(video, 0, 0, 900, 675);
               context.drawImage(logo, 50, 736.5, 800,402);
               

               $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');
               $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


           gifPhotos();

        }, 4000 )

        setTimeout(function(){
            $('#photos').css('opacity', '0');
            setTimeout(function(){
                $('#photos').fadeOut();
            }, 1100);

            showGifs();
            console.log('hi')
            $(this).css('pointer-events', 'all');
        }, 10000);
    });



    // PHOTO CLICK
    document.getElementById("snap_photo").addEventListener("click", function() {
        $(this).fadeOut(1000);
        $(this).css('pointer-events', 'none');
        var pics = []

        countDown();
            // take the picture
            setTimeout(function(){
                $('#pictures').append("<div class='canvas_holder photo'><canvas id='pic' class='canvas' width='900' height='1200'></canvas></div>")
                
                var canvas = $('#pic');
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                var logo = $('.logo_card')[0];
                var back = $('.back')[0];
               
               context.drawImage(back, 0, 0, 900, 1200);
               context.drawImage(video, 0, 0, 900, 675);
               context.drawImage(logo, 50, 736.5, 800,402);
               

                $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


                $('#pictures').append("<canvas id='draw' class='drawing' width='900' height='1200'></canvas>");
                var drawcanvas = $('#draw');
                var ctx = drawcanvas[0].getContext('2d');
                var video = document.getElementById('video');
                var logo = $('.logo_card')[0];
                var back = $('.back')[0];
               
               ctx.drawImage(back, 0, 0, 900, 1200);
               ctx.drawImage(video, 0, 0, 900, 675);
               ctx.drawImage(logo, 50, 736.5, 800,402);






            }, 4000);

        setTimeout(function(){
            $('#photos').slideUp()
            $('.submit').delay(1000).fadeIn();
            // $('.print').delay(1000).fadeIn();
            $(this).css('pointer-events', 'all');
            showPictures();
        }, 5000);
    });



    // DRAWING
    $('#pen').click(function(){
        // $('.canvas_holder').append("<canvas id='draw' width='900' height='1200'></canvas>")
       context = document.getElementById('draw').getContext("2d");
        $('#draw').fadeIn()
        $('.draw_ctl').fadeIn();
        $('.colors').fadeIn();
        $('.sizes').fadeIn();
        $('#pictures .canvas_holder img').hide();

        can = document.getElementById('draw');
        can.addEventListener('mousedown', onMouseDown);
        can.addEventListener('touchstart', onTouchStart);
        can.addEventListener('mousemove', onMouseMove);
        can.addEventListener('touchmove', onTouchMove);
        can.addEventListener('mouseup', onMouseEnd);
        can.addEventListener('touchend', onMouseEnd);
        can.addEventListener('mouseleave', onMouseEnd);
        can.addEventListener('touchleave', onMouseEnd);
        $(this).fadeOut();
    });



    function onMouseDown (e){

      var mouseX = e.pageX - this.offsetLeft;
      var mouseY = e.pageY - this.offsetTop;
      console.log(e)

            
      paint = true;
      addClick(e.clientX - this.offsetLeft, e.clientY - this.offsetTop);
      redraw();
      onTouchMove(e);
    };

    function onTouchStart (e){
        addClick(e.touches[0].screenX - this.offsetLeft, e.touches[0].screenY - this.offsetTop);
        redraw();
      
    };

    function onMouseMove (e){
        if(paint){
            addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
            redraw();
            onTouchMove(e)
      }
      
    };

    function onTouchMove (e){
        addClick(e.touches[0].screenX - this.offsetLeft, e.touches[0].screenY - this.offsetTop, true);
        redraw();
    };

    function onMouseEnd (e){
         paint = false;
    };



    var clickX = new Array();
    var clickY = new Array();
    var clickDrag = new Array();
    var paint;
    var colorWhite = "#ffffff";
    var colorGreen = "#49a463";
    var colorBlue = "#30b9dc";
    var colorBlack = "#000000";
    var colorRed = "#c22628";

    var curColor = colorRed;
    var clickColor = new Array();

    $('#colorWhite').click(function(){
        curColor = colorWhite
    });

    $('#colorGreen').click(function(){
        curColor = colorGreen
    });

    $('#colorBlue').click(function(){
        curColor = colorBlue
    });

    $('#colorBlack').click(function(){
        curColor = colorBlack
    });

    $('#colorRed').click(function(){
        curColor = colorRed
    });

    $('.color').click(function(){
        $('.canvas_holder').css('background-color', curColor);
        setTimeout(function(){
            $('.canvas_holder').css('background-color', 'transparent');
        },200)
    });


    var small = 5;
    var medium = 13;
    var large = 20;
    var huge = 30;

    var curSize = medium;
    var clickSize = new Array();

    $('#small').click(function(){
        curSize = small;
    });

    $('#medium').click(function(){
        curSize = medium;
    });

    $('#large').click(function(){
        curSize = large;
    });

    $('#huge').click(function(){
        curSize = huge;
    });



    

    function addClick(x, y, dragging)
    {
      clickX.push(x);
      clickY.push(y);
      clickDrag.push(dragging);
      clickColor.push(curColor);
      clickSize.push(curSize);
    }

    function redraw(){
      // context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
      
      context.strokeStyle = "#df4b26";
      context.lineJoin = "round";
      

                
      for(var i=0; i < clickX.length; i++) {        
        context.beginPath();
        if(clickDrag[i] && i){
          context.moveTo(clickX[i-1], clickY[i-1]);
         }else{
           context.moveTo(clickX[i]-1, clickY[i]);
         }
         context.lineTo(clickX[i], clickY[i]);
         context.closePath();
         context.strokeStyle = clickColor[i];
         context.lineWidth = clickSize[i];
         context.stroke();
      }
    }


    $('.draw_ctl').click(function(){
      clickX = new Array();
      clickY = new Array();
      clickDrag = new Array();
      clickColor = new Array();
      clickSize = new Array();
      context.clearRect(0, 0, 900, 1200);

      var drawcanvas = $('#draw');
      var ctx = drawcanvas[0].getContext('2d');
      var image = $('#pictures .canvas_holder img')[0];
      var logo = $('.logo_card')[0];
      var back = $('.back')[0];
               
       ctx.drawImage(back, 0, 0, 900, 1200);
       ctx.drawImage(video, 0, 0, 900, 675);
       ctx.drawImage(logo, 50, 736.5, 800,402);
       ctx.drawImage(image, 0, 0, 900, 1200);
    });


});



