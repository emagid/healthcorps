$(document).ready(function(){
    // no rights clicks
    document.addEventListener('contextmenu', event => event.preventDefault());




    // page load animation
    $('.welcome').fadeIn();

    setTimeout(function(){
        $('.logo').css('margin-top', '72px');
        $('.logo').css('height', '70px');
    }, 2000 );

    setTimeout(function(){
        $('.multiply').css('background-color', 'rgba(255, 255, 255, 0)');
    },3300 );

    setTimeout(function(){
        $('#choose_picture').fadeIn();
        $('#choose_gif').fadeIn();
        $('.photos .button').css('display', 'flex');
        $('.event_pics').fadeIn();
    }, 4500);

    $('.logo').click(function(){
        window.location.href = '/';
    })


    // event pics Button slide
    $('.event_pics .overlay').on('click', function(){
        $('.photos').css('opacity', '0');

        $(this).children('h2').fadeOut(1500);
        setTimeout(function(){
            $('.inner_content').css('display', 'flex');
            $('.event_pics').css('height', '1920px');
            $('.event_pics').addClass('active');

            $('.photos').hide();
            $('.inner_content').fadeIn(2000);
            // $('.display_images').slideDown();
            $('.choice').css('opacity', '1');

        }, 2000);

        setTimeout(function(){
            $('#home_click').fadeIn();
            $('.event_pics').css('min-height', '1920px');
            $('.event_pics').css('touch-events', 'none');
        }, 1500);
    });


    // Home click
    $('#home_click').on('click', function(){
        $('#jQKeyboardContainer').remove()

            $('.photos').removeClass('photo_booth_active')
            $(this).fadeOut();
            $('.inner_content').fadeOut();
            $('#donate_form')[0].reset();
    });


    $('.fa-close').click(function(){
        $('.share_overlay').slideUp();
        $('#jQKeyboardContainer').slideUp();
        $('.fa-close').fadeOut();
    })




    // Take Photo click
     $('#choose_picture').on('click', function(){
        $(this).addClass('photo_booth_active')
        $('#photos').css('margin-bottom', '0');
        $('.photos').css('background-color', 'rgba(36, 31, 32, 0.82)');
        $('.event_pics').fadeOut();
        $('.photos .button').fadeOut();
        setTimeout(function(){
            $('.donate').hide();
            $('.about').hide();
            $('.event_pics').hide();

            $('.photos').css('height', '1920px');
            $('#video').fadeIn();
            $('#snap_photo').fadeIn();
            $('.pic_text').fadeIn();
            $('#home_click').fadeIn();
        }, 1000);
    });


    // Take Gif click
   $('#choose_gif').on('click', function(){
        $(this).addClass('photo_booth_active')
        $('#photos').css('margin-bottom', '0');
        $('.photos').css('background-color', 'rgba(36, 31, 32, 0.82)');
        $('.event_pics').fadeOut();
        $('.photos .button').fadeOut();
        setTimeout(function(){
            $('.donate').hide();
            $('.about').hide();
            $('.event_pics').hide();

            $('.photos').css('height', '1920px');
            $('#video').fadeIn();
            $('#snap_gif').fadeIn();
            $('.gif_text').fadeIn();
            $('#home_click').fadeIn();
        }, 1000);
    });




     // Checkmark
    var count = (function (num) {
        var counter = 0;
        return function (num) {return counter += num;}
    })();
    

     $(document).on('click', '.gif', function(){
        var num = count(1);
        if ( !$(this).hasClass('checked') && num < 5 ) {            
            if ( num <= 4  ) {
                $(this).css('outline', 'rgb(228, 0, 43) solid 5px')
                $(this).addClass('checked');
                if ( num  === 4 ) {
                    $('.gif_info').fadeOut();
                    $('.next').slideDown();
                    $('html, body').animate({
                        scrollTop: $(".home").offset().top
                    }, 500);
                }
            }

        }else if ( $(this).hasClass('checked') ) {
            num = count(-2);
            $(this).removeClass('checked');
            $(this).css('outline', '0px')
            if ( num  === 4 ) {
                    $('.next').slideDown();
                    $('.gif_info').fadeOut();
                    $('html, body').animate({
                        scrollTop: $(".home").offset().top
                    }, 500);

            }else {
                $('.next').slideUp();
                $('.gif_info').fadeIn();

            }

        }else {
            num = count(-1);
        }
     });




     // Gif Next click
     function picAction( pic ) {
      $(pic).fadeIn('fast');
      $(pic).delay(1300).fadeOut(400);
    }

     function gif( pics ) {
      var offset = 0

      for ( i=0; i<2; i++ ) {
        pics.each(function(){
          var timer
          var self = this;
          timer = setTimeout(function(){
              picAction(self)
          }, 0 + offset);

          offset += 1500;
        });
      }

      setTimeout(function(){
            var gif_pics = $('.gif_show').children('.checked');
            gif( gif_pics )
        }, 12000);
    }

     $(document).on('click', '.next', function(){
        $('#pictures').fadeOut();
        $('.gif_show').show();
        $('.gif_show').addClass('flex');

        // =========  NEED TO TURN IMAGES INTO GIF TO SHOW HERE  ================
        $('#submit_form').find('.image_encoded').remove();
        $('.canvas_holder.checked img').each(function () {
            $('#submit_form').append($('<input type="hidden" name="images[]" value="'+$(this).attr('src')+'">'));
        });
        let deferreds = [];
        let imgs = [];
        $("#submit_form input[name='images[]']").each(function(i,el){
            deferreds.push(
                $.post('/contact/make_frame/', {
                    image: el.value
                },function(data) {
                    imgs.push(window.location.href+'content/uploads/Snapshots/'+data.image.image);
                    $('input[name="images[]"]')[i].value = data.image.id;
                }));
        });

        //Show loading image
        $.when(...deferreds).then( function() {
             gifshot.createGIF({
                 'images': imgs,
                 'frameDuration':4,
                 'gifWidth': 900,
                 'gifHeight': 1200
             },function(obj) {
                 if(!obj.error) {
                     var image = obj.image;
                     $('#loader').fadeOut();
                     $('.gif_show').append("<img src=' " + image + "'>")
                     $('#submit_form').append($('<input type="hidden" name="gif" value="'+image+'">'));
                     $.post('/contact/save_img/',$('#submit_form').serialize(),function(data){
                         $('#submit_form input[name=gif]').val(data.gif.id);
                     });
                 }
             });
             // imgs = imgs.join();

             // imgs = 'http://cdn4.gurl.com/wp-content/uploads/2014/03/woman-pointing-at-self.jpg,https://thumb1.shutterstock.com/display_pic_with_logo/1729711/280157228/stock-photo-model-isolated-pointing-to-herself-280157228.jpg,http://tse4.mm.bing.net/th?id=OIP.hz6jv2fJu8OPK4x6s6ybbAEsDI&w=300&h=200&pid=1.1,https://idc-static.s3.amazonaws.com/seo/pointing%20at%20myself.jpg';

             // $.ajax('https://udayogra-images-to-gif-converter-v1.p.mashape.com/am',
             //     {
             //         type: 'GET',
             //         data: {
             //             delay: 500,
             //             imageurls: imgs
             //         },
             //         datatype: 'json',
             //         beforeSend: function(xhr) {
             //             xhr.setRequestHeader("X-Mashape-Authorization", "TbbShja6Qtmsh8WPaTF6sb6GCXmbp18UoSRjsnja4sFYJCwcjc");
             //         },
             //         success: function(data) {
             //             //remove loading image
             //             $('#loader').fadeOut();
             //             var giflink = data.giflink;  //gifurl
             //             $('.gif_show').append("<img src=' " + giflink + "'>")
             //             $('#submit_form').append($('<input type="hidden" name="gif" value="'+giflink+'">'));
             //             $.post('/contact/save_img/',$('#submit_form').serialize(),function(data){
             //                 $('#submit_form input[name=gif]').val(data.gif.id);
             //             });
             //         }
             //     });
         });


        $('.submit').delay(1000).fadeIn();
        // $('.like').hide();

        // var pics = $('#pictures').children('.checked');

        // for ( i=0; i<4; i++ ) {
        //     $('.gif_show').append(pics[i]);
        // }

        // var gif_pics = $('.gif_show').children('.checked');
        //         // console.log(gif_pics)

        // // setting up order for gif loop
        // $('.gif_show').children('.checked').hide();
        // // $($('.gif_show').children('.checked')[0]).show();

        // for ( i=0; i<4; i++ ) {
        //     $('.gif_show').removeClass('checked');
        //     $($($('.gif_show').children()[i]).children()[0]).hide();
        //     // $($('.checked').children()[0]).hide();        
        // }

        // gif( gif_pics )
     });
    

     // Share click
     $(document).on('click', '.submit', function(){
       
        if ( $('#draw').is(':visible') ) {
             saveDrawing();


            $.post('/contact/save_img/',{image:$('#pictures > img:last-child').attr('src')},function(data){
                $('#submit_form input[name=image]').val(data.image.id);
            });
        }

        $('.share_overlay').slideDown();
        $('.fa-close').slideDown();
        
     });

     $('#share_btn').click(function(){
        $('.fa-close').fadeOut();
        $('.jQKeyboardContainer').fadeOut();
     })

     function saveDrawing() {
            
            var draw_canvas = $('#draw');
            $($(draw_canvas[0]).parents('#pictures')[0]).append(convertCanvasToImage(draw_canvas[0]))

            $('.drawing').hide();
        }
     


     // Form submit
     $('#submit_form').on('submit', function(e){
        e.preventDefault();
            $('#share_alert').slideDown();
            $('#share_alert').css('display', 'flex');
        $.post('/contact', $(this).serialize(), function (response) {
            $('#submit_form')[0].reset();

            setTimeout(function(){
                 window.location.href = '/';
            }, 3000);
        });
     });


     // Click of done sharing
     $('#done').on('click', function(e){
        $('.share_overlay').slideUp();
        $('#jQKeyboardContainer').remove()
        // remove all images
        $('#pictures').children('.canvas_holder');
     });


     // click of showing images
     $('.event_images').on('click', function(){
        $('.choice').fadeOut();
        $('#event_gifs').fadeIn();
        $('.display_images').slideDown();
     });


      // click of showing gifs
     $('.event_gifs').on('click', function(){
        $('.choice').fadeOut();
        $('#event_images').fadeIn();
        $('.display_gifs').slideDown();
     });


      // click of showing gifs when images are shown
     $('#event_gifs').on('click', function(){
        $('#event_images').fadeIn();
        $('#event_gifs').fadeOut();
        $('.display_images').slideUp();
        setTimeout(function(){
            $('#event_images').fadeIn();
            $('#event_images').addClass('shrunk');
            $('.display_gifs').slideDown();
        }, 1000)
     });

     // click of showing images when gifs are shown
     $('#event_images').on('click', function(){
        $('#event_gifs').fadeOut();
        $('#event_images').fadeOut();
        $('.display_gifs').slideUp();
        setTimeout(function(){
            $('#event_gifs').fadeIn();
            $('#event_gifs').addClass('shrunk');
            $('.display_images').slideDown();
        }, 1000)
     });


     $('.print').click(function(){
          saveDrawing();  
          var source = $($('#pictures img:last-child')[1]).attr('src');
          VoucherPrint(source);
     });


     function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }

    $('.top').on('click', function(){
        $('.display_images').animate({
                scrollTop: $(".display_images img:nth-child(2)").offset().top
        }, 2000);

    });

    $('.top').on('click', function(){
        $('.display_gifs').animate({
                scrollTop: $(".display_gifs img:nth-child(2)").offset().top
        }, 2000);

    });


     function VoucherSourcetoPrint(source) {
      return "<html><head><title>Powered by Popshap.com</title><style>/* style sheet for 'A4' printing */"+
        "@page {" +
         "size: A4;" +
         "margin: 0%;"+
         "}"+ 
         "</style><script>function step1(){\n" +
          "setTimeout('step2()', 10);}\n" +
          "function step2(){window.print();window.close()}\n" +
          "</script></head><body onload='step1()'>\n" +
          "<img src='" + source + "' /></body></html>";
    }
    function VoucherPrint(source) {
      Pagelink = "about:blank";
      var pwa = window.open(Pagelink, "_new");
      pwa.document.open();
      pwa.document.write(VoucherSourcetoPrint(source));
      pwa.document.close();
    }



    var timer;
        $(".directory ul li a").on({
             'click': function clickAction(e) {
              e.preventDefault();
                 var self = this;
                 var link = $(this).attr("href");
                    $(self).children('.square').css('transform', 'scale(.9)');
                  timer = setTimeout(function () {
                      $(self).children('.square').css('transform', 'scale(1)');
                  }, 100);
                  timer = setTimeout(function () {
                      window.location.href = link;
                  }, 500);
             }
        });


    var timer;
        $('.photos .button').on({
            'click': function clickAction(e){
                e.preventDefault();
                var self = this;
                 var link = $(this).attr("href");
                $(self).addClass('clicked');
                timer = setTimeout(function () {
                      // window.location.href = link;
                  }, 500);

            }
        });



     // initial timeout redirect homepage
        var initial = null;

        function invoke() {
            initial = window.setTimeout(
                function() {
                    window.location.href = '/';
                }, 60000);
        }

        invoke();

        $('body').on('click mousemove', function(){
            window.clearTimeout(initial);
            invoke();
        });

});



