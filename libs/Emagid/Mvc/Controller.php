<?php

namespace Emagid\Mvc; 

/**
* Base class for controllers
*/
class Controller{

	/**
	* @var string
	* template to load. Will effect the load_view method
	*/
	public $template = null;

	/**
	* @var string 
	* name of active controller
	*/
	public $name = 'home' ;

	/**
	* @var string 
	* name of active view
	*/
	public $view = 'index';

	/**
	* @var string 
	* current area (area may include controllers , views, etc... )
	*/
	public $area = null ; 

	/**
	* @var Object
	* Utilities class 
	*/
	public $emagid = [] ;



	public function __construct(){
		global $emagid; 

		if($emagid->template)
			$this->template = $emagid->template;

		$this->emagid = $emagid; 
	}


	/**
	* load the view 
	*
	* @param string $view 	name of view to load. default is the class's view
	* @param object $model 	object that contains all the data for the view.
	*         
	*/
	public function loadView(){
		global $emagid ; 

		$view = null ;
		$model = null ;

		$args = func_get_args();
    
		if (count($args)){

			if(is_string($args[0])){
				$view = $args[0];

        array_shift($args);
				//unset($args[0]);  need a array_shift to set the first element to 0
			}

			if (count($args)){
				$model = $args[0];
			}


		}

		if($view)
			$this->view = $view; 

		if($this->template){
			$path = 'templates/'.$this->template.'/'.$this->template.'.php';
			require($path);
		}else{

			$this->model = $model ;
			$this->renderBody($model);
		}

	}


	/** 
	* Will render view's contetnt in the template 
	*/
	public function renderBody($model = null){
		global $emagid ; 

		$path = $this->getViewPath(); 

		if (file_exists($path)){		
			if(!include($path)){
				die("<h1>Failed to load the view : ".$path."</h1>");
			}
		}
		else {
			echo ("<strong>ERROR : </strong>Could not find file : ".$path);
			//d (\Emagid\Mvc\Mvc::$route);

		}
	}


	public function getViewPath(){
		return implode(DIRECTORY_SEPARATOR, array_filter(['views', $this->area, $this->name, $this->view])).'.php';

	}



	/** 
	* Returns an Html renderable object 
	*/ 
	public function view(){
		$html = new \Emagid\Mvc\Views\Html(func_get_args());

		return $html;
	}

	/** 
	* Returns an Html renderable object 
	*/ 
	public function json(){
		$json = new \Emagid\Mvc\Views\Json(func_get_args());

		return $json;
	}

}

?>