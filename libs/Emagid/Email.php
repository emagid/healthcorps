<?php 

namespace Emagid;


/**
* The Email class provides an easy handling of email templates .
*/ 
class Email {

	var $subject; 

	var $to = []; 

	var $body;

	/** 
	* Initialize the email object.
	*/
	public static function init($params = []){
		$email = new Email($params); 
		return $email; 
	}



	public function addTo($to, $type = 'to'){
		$this->to[] = $this->fixTo($to, $type); 

		return $this;		
	}

	public function addCc($to){
		return $this->addTo($to, 'cc'); 
	}

	public function addBcc($to){
		return $this->addTo($to, 'bcc'); 
	}


	public function send(){
		if( class_exists('\Mandrill')) {
			global $emagid; 

			$mandrill = new \Mandrill($emagid->email->api_key);
		} else {
			throw new \Exception('Could not find Mandrill class. You may include it in the config array when instantiating Emagid;');
			exit();
		}

		$this->from = (array)$emagid->email->from;
    	

		$message_arr = array(
            'html' => $this->body,
            'subject' => $this->subject,
            'from_email' => $this->from['email'],
            'from_name' => $this->from['name'] ,
            'to' => $this->to,
        	'headers' => array('Reply-To' => $this->from['email']),
        );

		$async = false;

	    $ip_pool = 'Main Pool';

	    $result = $mandrill->messages->send($message_arr, $async, $ip_pool);
	}



	/**
	* Reformat the email address to fit the medium we will use to send. 
	*/ 
	private function fixTo($to, $type) {
		if (is_array($to)){
			$to['type'] = $type; 

			return $to; 
		} else {
			return [
				'email' => $to, 
				'type'  => $type
			];
		}

	}



	public function subject($subject){
		$this->subject = $subject; 
		return $this;
	}

	public function template($template, $model, $debug = false){
		ob_start();
        include $template;
        $this->body = ob_get_contents();

        if ($debug)  {
        	die($this->body); 
        }

        
        ob_end_clean();

        return $this;
	}

  	public function __construct($params = []) {

	}
}