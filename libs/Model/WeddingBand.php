<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/20/15
 * Time: 1:06 PM
 */

namespace Model;


class WeddingBand extends \Emagid\Core\Model {
    public static $tablename = "wedding_band";

    public static $fields = [
        "active", "name", "slug", "price", "category",
        "diamond_color", "diamond_quality", "video_link",
        'colors', 'metals', 'metal_colors',
        'setting_labor',
        'polish_labor',
        'head_to_add',
        'melee_cost',
        'shipping_from_cast',
        'shipping_to_customer',
        'total_14kt_cost',
        'total_18kt_cost',
        'total_plat_cost',
        'options',
        'setting_style',
        'alias',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    static $relationships = [
    ];

    public static $searchFields = ['name'];

    public static function searchMetaData()
    {
        $data = [];
        $objects = self::getList();
        foreach($objects as $object){
            $fieldData = [];
            foreach(self::$searchFields as $field){
                $fieldData[] = $object->$field;
            }

            $data[] = ['fields' => $fieldData, 'object' => $object];
        }

        return $data;
    }

    public function generateSearchResult()
    {
        return ['name' => $this->name, 'image' => $this->featuredImage(), 'link' => "/weddingbands/{$this->slug}"];
    }

    public function videoLink()
    {
        if($folderName = $this->getVideoFolderName()){
            $videoArray = [];
            $videos = glob(UPLOAD_PATH."videos/$folderName/" . "*.mp4");
            foreach($videos as $video){
                $video = str_replace(UPLOAD_PATH, UPLOAD_URL, $video);
                $videoArray[] = $video;
            }

            return $videoArray;
        } elseif($this->video_link){
            return [UPLOAD_URL.'weddingbands/'.$this->video_link];
        } else {
            return null;
        }
    }

    public function categories()
    {
        return Product_Category::getList(['where' => ['wedding_band_id' => $this->id]]);
    }

    public function getCategoryIds()
    {
        $categories = $this->categories();
        $data = [];
        foreach($categories as $category){
            $data[] = $category->category_id;
        }

        return $data;
    }

    public function getImageFolderName()
    {
        $folderName = "{$this->slug}-W";
        if(file_exists(UPLOAD_PATH."images/$folderName")){
            return $folderName;
        } elseif(file_exists(UPLOAD_PATH."images/{$this->slug}")) {
            return $this->slug;
        } else {
            return false;
        }
    }

    public function getVideoFolderName()
    {
        $folderName = "{$this->slug}-E";
        if(file_exists(UPLOAD_PATH."videos/$folderName")){
            return $folderName;
        } elseif(file_exists(UPLOAD_PATH."videos/{$this->slug}")) {
            return $this->slug;
        } else {
            return false;
        }
    }
    
    public function featuredImage()
    {
        if($foldName = $this->getImageFolderName()){
            return UPLOAD_URL."images/$foldName/$foldName.jpg";
        } else {
            $productImage = new Product_Image();
            $productImage = $productImage->getProductImage($this->id, 'WeddingBand');
            if(!empty($productImage)){
                return UPLOAD_URL."products/".$productImage[0]->image;
            }
            else return FRONT_ASSETS."img/ring5.png";
        }
    }

    public function getImages()
    {
        if($foldName = $this->getImageFolderName()){
            $images = glob(UPLOAD_PATH."images/$foldName/" . "*.jpg");
            $imageArray = [];
            foreach($images as $image){
                $image = str_replace(UPLOAD_PATH, UPLOAD_URL, $image);
                $imageArray[] = $image;
            }

            $setFolderName = $this->slug.'-E';
            $setImages = glob(UPLOAD_PATH."images/$setFolderName/" . "*.set.*");
            foreach($setImages as $image){
                $image = str_replace(UPLOAD_PATH, UPLOAD_URL, $image);
                $imageArray[] = $image;
            }
        } else {
            $productImage = new Product_Image();
            $productImage = $productImage->getProductImage($this->id, 'WeddingBand');
            $imageArray = [];
            if($productImage){
                // get rid of featured image
                foreach($productImage as $image){
                    $imageArray[] = UPLOAD_URL."products/".$image->image;
                }
            }
        }
        return $imageArray;
    }

    public function getMap()
    {
        $map = new Product_Map();
        return $map->getMap($this->id, 'WeddingBand');
    }

    public function getPrice($selectMetal, $stone)
    {
        $price = $this->getPrices($stone);

        switch(strtoupper($selectMetal)){
            case '14KT GOLD':
                return $price['14kt_gold'];
            case '18KT GOLD':
                return $price['18kt_gold'];
            case 'PLATINUM':
                return $price['Platinum'];
        }
    }

    public function getPriceDiff($selectMetal, $stone)
    {
        $price = $this->getPrices($stone);

        switch(strtoupper($selectMetal)){
            case '14KT GOLD':
                return 0;
            case '18KT GOLD':
                return $this->formatPrice($price['18kt_gold']) - $this->formatPrice($price['14kt_gold']);
                break;
            case 'PLATINUM':
                return $this->formatPrice($price['Platinum']) - $this->formatPrice($price['14kt_gold']);
        }
    }

    private function formatPrice($string)
    {
        $string = str_replace(['$', ','], '', $string);
        $string = trim($string);
        return floatval($string);
    }

    public static function getColors(){
        return ['White','Yellow','Rose'];
    }

    public static function getMetals(){
        return ['14kt gold', '18kt gold', 'Platinum'];
    }

    public function getUrl()
    {
        return "/weddingbands/{$this->slug}";
    }

    public function getOptions()
    {
        return json_decode($this->options, true);
    }

    public function getDefaultOption($getStone = 0)
    {
        $data = $this->getOptions();
        $keys = array_keys($data);

        return $data[$keys[$getStone]];
    }

    public function getPrices($stone)
    {
        $options = $this->getOptions();

        if(is_numeric($stone)){
            $key = array_keys($options)[$stone];
            return $options[$key]['price'];
        } else {
            return $options[$stone]['price'];
        }
    }

    public function getDefaultStone($getStone = 0)
    {
        $data = $this->getOptions();
        $keys = array_keys($data);
        return $keys[$getStone];
    }

    public static function randomizer($limit = 10){
        $items = self::getList();
        shuffle($items);
        return array_slice($items,0,$limit);
    }
}