<?php

class gifsController extends adminController {
	
	function __construct(){
		parent::__construct("Gif");
	}

    public function update_post()
    {
        $obj = \Model\Gif::getItem($_POST['id']);
        $obj->in_slider = $_POST['in_slider'];

        if ($obj->save()) {
            $this->toJson(['status'=>true]);
        }
    }
  
}